#include "opencont.h"
using namespace std;
using namespace cv;

opencont::opencont()
{

}

void opencont::countors_rgb(char* filename, char* savefile){
    //getting an image
    IplImage* image = 0;
    IplImage* gray = 0;
    IplImage* bin = 0;
    IplImage* dst = 0;
    IplImage* dst2 = 0;

            image = cvLoadImage(filename,1);

            printf("[i] image: %s\n", filename);
            assert( image != 0 );

            // creating one-channel pictures
            gray = cvCreateImage( cvGetSize(image), IPL_DEPTH_8U, 1 );
            bin = cvCreateImage( cvGetSize(image), IPL_DEPTH_8U, 1 );
            //clone to work with them
            dst = cvCloneImage(image);
            dst2 = cvCloneImage(image);

            //window to show the contours

            cvNamedWindow("contours",CV_WINDOW_AUTOSIZE);
            cvNamedWindow("contours2",CV_WINDOW_AUTOSIZE);
            //findCircles(dst2);

            //CV:: converting to grayscale
            cvCvtColor(image, gray, CV_RGB2GRAY);

            //CV:: converting to binary image
            cvInRangeS(gray, cvScalar(40), cvScalar(140), bin); // atoi(argv[2])
            //Mat ca = cvarrToMat(gray);
            //Mat bi;
            cvCanny(gray, bin, 50.0, 200.0, 3);
            //IplImage* bin2 = cvCloneImage(&(IplImage)bi);
            CvMemStorage* storage = cvCreateMemStorage(0);
            CvSeq* contours=0;

            //CV:: finding contours
            int contoursCont = cvFindContours( bin, storage,&contours,sizeof(CvContour),CV_RETR_LIST,CV_CHAIN_APPROX_SIMPLE,cvPoint(0,0));

            //CV:: drawing them
            for(CvSeq* seq0 = contours;seq0!=0;seq0 = seq0->h_next){
                    cvDrawContours(dst, seq0, CV_RGB(255,255,255), CV_RGB(255,255,255), 0, 1, 8); // рисуем контур
            }

            cvShowImage("contours", dst);
            cvShowImage("contours2", bin);
            //filename = "Real.jpg";
            //char* savefile1 = savefile+"Dst.jpg";
            //char* savefile2 = savefile+"";
            //cvSaveImage(savefile, dst);
            //cv::imwrite(savefile,dst);

            //cvSaveImage("RealRgbBin.jpg", bin);
            //minusCont(filename);
            cvWaitKey();
            //amcap *capt = new amcap();
            //capt->EnclosingCircle(dst);

            cvReleaseImage(&image);
            cvReleaseImage(&gray);
            cvReleaseImage(&bin);
            cvReleaseImage(&dst);
            cvReleaseImage(&dst2);
            cvReleaseMemStorage(&storage);

            //cvDestroyAllWindows();
            cvDestroyWindow("contours");
            cvDestroyWindow("contours2");

}

void opencont::countors_dpt(char* filename, char* savefile){


    IplImage* image = 0;
    IplImage* gray = 0;
    IplImage* bin = 0;
    IplImage* dst = 0;
    IplImage* dst2 = 0;

            image = cvLoadImage(filename,1);

            printf("[i] image: %s\n", filename);
            //assert( image != 0 );


            gray = cvCreateImage( cvGetSize(image), IPL_DEPTH_8U, 1 );
            bin = cvCreateImage( cvGetSize(image), IPL_DEPTH_8U, 1 );

            dst = cvCloneImage(image);
            dst2 = cvCloneImage(image);

            cvNamedWindow("contours",CV_WINDOW_AUTOSIZE);
            cvNamedWindow("contours2",CV_WINDOW_AUTOSIZE);
            //findCircles(dst2);

            cvCvtColor(image, gray, CV_RGB2GRAY);

            cvInRangeS(gray, cvScalar(40), cvScalar(140), bin); // atoi(argv[2])
            //Mat ca = cvarrToMat(gray);
            //Mat bi;
            cvCanny(gray, bin, 50.0, 200.0, 3);
            //IplImage* bin2 = cvCloneImage(&(IplImage)bi);
            CvMemStorage* storage = cvCreateMemStorage(0);
            CvSeq* contours=0;

            int contoursCont = cvFindContours( bin, storage,&contours,sizeof(CvContour),CV_RETR_LIST,CV_CHAIN_APPROX_SIMPLE,cvPoint(0,0));

            for(CvSeq* seq0 = contours;seq0!=0;seq0 = seq0->h_next){
                    cvDrawContours(dst, seq0, CV_RGB(255,255,255), CV_RGB(255,255,255), 0, 1, 8); // рисуем контур
            }

            //cvShowImage("contours", dst);
            //cvShowImage("contours2", bin);
            //filename = "Real.jpg";
            //char* savefile1 = savefile+"Dst.jpg";
            //char* savefile2 = savefile+"";
            cvSaveImage(savefile, dst);
            cvSaveImage("RealDptBin.jpg", bin);
            minusContDpt(filename);
            cvWaitKey();
            //amcap *capt = new amcap();
            //capt->EnclosingCircle(dst);
            cvReleaseImage(&image);
            cvReleaseImage(&gray);
            cvReleaseImage(&bin);
            cvReleaseImage(&dst);
            cvReleaseImage(&dst2);
            cvReleaseMemStorage(&storage);
            cvDestroyAllWindows();

}


void opencont::minusCont(char* filename){
    IplImage* depth = cvLoadImage("RealRGB.jpg",1);
    IplImage* binary = cvLoadImage("RealRgbBin.jpg", 1);
    cvSub(depth, binary, binary);
    //cvNamedWindow( "sub", 1 );
    //cvShowImage( "sub", binary);
    cvSaveImage("SubReal.jpg", binary);
    EnclosingCircle(depth);
}
void opencont::minusContDpt(char* filename){
    IplImage* depth = cvLoadImage("RealDpt.jpg",1);
    IplImage* binary = cvLoadImage("RealDptBin.jpg", 1);
    cvSub(depth, binary, binary);
    //cvNamedWindow( "sub", 1 );
    //cvShowImage( "sub", binary);
    cvSaveImage("SubRealDpt.jpg", binary);
    EnclosingCircle(depth);
}

void opencont::EnclosingCircle(IplImage* _image)
{

        assert(_image!=0);

        IplImage* bin = cvCreateImage( cvGetSize(_image), IPL_DEPTH_8U, 1);

        cvConvertImage(_image, bin, CV_BGR2GRAY);
        cvCanny(bin, bin, 50, 200);

//        cvNamedWindow( "bin", 1 );
//        cvShowImage("bin", bin);

        CvMemStorage* storage = cvCreateMemStorage(0);
        CvSeq* contours=0;

        int contoursCont = cvFindContours( bin, storage,&contours,sizeof(CvContour),CV_RETR_LIST,CV_CHAIN_APPROX_SIMPLE,cvPoint(0,0));

        assert(contours!=0);

        for( CvSeq* current = contours; current != NULL; current = current->h_next ){
                CvPoint2D32f center;
                float radius=20;
                cvMinEnclosingCircle(current, & center, &radius);
                cvCircle(_image, cvPointFrom32f(center), radius, CV_RGB(255, 0, 0), 1, 8);
        }


        cvReleaseMemStorage(&storage);
        cvReleaseImage(&bin);
}

