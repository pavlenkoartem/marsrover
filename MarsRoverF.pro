QT += core
QT -= gui
QT += serialport

CONFIG += c++11
QT_CONFIG -= no-pkg-config
CONFIG  += link_pkgconfig
PKGCONFIG += opencv


TARGET = MarsRoverF
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    serialport.cpp \
    opencont.cpp \
    taking.cpp \
    textprogram.cpp

HEADERS += \
    serialport.h \
    opencont.h \
    taking.h \
    textprogram.h

INCLUDEPATH += /opt/local/include/libusb-1.0
INCLUDEPATH += /usr/local/Cellar/opencv/3.4.3/include

LIBS += -L/usr/local/lib \
    -lopencv_stitching \
    -lopencv_superres \
    -lopencv_videostab \
    -lopencv_aruco \
    -lopencv_bgsegm \
    -lopencv_bioinspired \
    -lopencv_ccalib \
    -lopencv_dnn_objdetect \
    -lopencv_dpm -lopencv_face \
    -lopencv_photo \
    -lopencv_fuzzy \
    -lopencv_hfs \
    -lopencv_img_hash \
    -lopencv_line_descriptor \
    -lopencv_optflow \
    -lopencv_reg \
    -lopencv_rgbd \
    -lopencv_saliency \
    -lopencv_stereo \
    -lopencv_structured_light \
    -lopencv_phase_unwrapping \
    -lopencv_surface_matching \
    -lopencv_tracking \
    -lopencv_datasets \
    -lopencv_dnn \
    -lopencv_plot \
    -lopencv_xfeatures2d \
    -lopencv_shape \
    -lopencv_video \
    -lopencv_ml \
    -lopencv_ximgproc \
    -lopencv_calib3d \
    -lopencv_features2d \
    -lopencv_highgui \
    -lopencv_videoio \
    -lopencv_flann \
    -lopencv_xobjdetect \
    -lopencv_imgcodecs \
    -lopencv_objdetect \
    -lopencv_xphoto \
    -lopencv_imgproc \
    -lopencv_core \
    -lfreenect
