#pragma once
#include <iostream>
#include <vector>
#include <cmath>
#include <QObject>
#include <QTimer>
#include <fstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <QCoreApplication>
#include <QThread>
#include <QMutex>
#include <pthread.h>
#include "taking.h"

using namespace cv;
using namespace std;


class myMutex {
    public:
        myMutex() {
            pthread_mutex_init( &m_mutex, NULL );
        }
        void lock() {
            pthread_mutex_lock( &m_mutex );
        }
        void unlock() {
            pthread_mutex_unlock( &m_mutex );
        }
    private:
        pthread_mutex_t m_mutex;
};


class MyFreenectDevice : public Freenect::FreenectDevice {
    public:
        MyFreenectDevice(freenect_context *_ctx, int _index)
            : Freenect::FreenectDevice(_ctx, _index), m_buffer_depth(FREENECT_DEPTH_11BIT),
            m_buffer_rgb(FREENECT_VIDEO_RGB), m_gamma(2048), m_new_rgb_frame(false),
            m_new_depth_frame(false), depthMat(Size(640,480),CV_16UC1),
            rgbMat(Size(640,480), CV_8UC3, Scalar(0)),
            ownMat(Size(640,480),CV_8UC3,Scalar(0)) {

            for( unsigned int i = 0 ; i < 2048 ; i++) {
                float v = i/2048.0;
                v = std::pow(v, 3)* 6;
                m_gamma[i] = v*6*256;
            }
        }

        void VideoCallback(void* _rgb, uint32_t timestamp) {
            m_rgb_mutex.lock();
            uint8_t* rgb = static_cast<uint8_t*>(_rgb);
            rgbMat.data = rgb;
            m_new_rgb_frame = true;
            m_rgb_mutex.unlock();
        };


        void DepthCallback(void* _depth, uint32_t timestamp) {
            m_depth_mutex.lock();
            uint16_t* depth = static_cast<uint16_t*>(_depth);
            depthMat.data = (uchar*) depth;
            m_new_depth_frame = true;
            m_depth_mutex.unlock();
        }

        bool getVideo(Mat& output) {
            m_rgb_mutex.lock();
            if(m_new_rgb_frame) {
                cv::cvtColor(rgbMat, output, CV_RGB2BGR); //to detect convert to HSV
                m_new_rgb_frame = false;
                m_rgb_mutex.unlock();
                return true;
            } else {
                m_rgb_mutex.unlock();
                return false;
            }
        }

        bool getDepth(Mat& output) {
                m_depth_mutex.lock();
                if(m_new_depth_frame) {
                    depthMat.copyTo(output);
                    m_new_depth_frame = false;
                    m_depth_mutex.unlock();
                    return true;
                } else {
                    m_depth_mutex.unlock();
                    return false;
                }
            }
    private:
        std::vector<uint8_t> m_buffer_depth;
        std::vector<uint8_t> m_buffer_rgb;
        std::vector<uint16_t> m_gamma;
        Mat depthMat;
        Mat rgbMat;
        Mat ownMat;
        myMutex m_rgb_mutex;
        myMutex m_depth_mutex;
        bool m_new_rgb_frame;
        bool m_new_depth_frame;
};

bool die(false);
string filename_rgb("Rgb");
string filename_dpt("Dpt");
string suffix(".png");
string filename_real_rgb("RealRGB");
string filename_real_dpt("RealDpt");







Mat depthMat(Size(640,480),CV_16UC1);
Mat depthfL (Size(640,480),CV_8UC1);
Mat depthfC (Size(640,480),CV_8UC1);
Mat depthfR (Size(640,480),CV_8UC1);
Mat coloredDepthAll(480, 640, CV_8UC3);
Mat rgbMat(Size(640,480),CV_8UC3,Scalar(0));
bool leftk = false;
bool centk = false;
bool rightk = false;




taking::taking()
{

}

void taking::videoLoop(){
    Freenect::Freenect freenect;
    MyFreenectDevice &device = freenect.createDevice<MyFreenectDevice>(0);
    namedWindow("rgb",CV_WINDOW_AUTOSIZE);
    namedWindow("depth",CV_WINDOW_AUTOSIZE);
    device.startVideo();
    device.startDepth();
    int framecount = 0;
    cv::Mat coloredDepthL(480, 212, CV_8UC3);
    cv::Mat coloredDepthC(480, 212, CV_8UC3);
    cv::Mat coloredDepthR(480, 214, CV_8UC3);
    uint16_t t_gamma[2048];
    uint16_t minDepthL;
    uint16_t minDepthC;
    uint16_t minDepthR;
    const uint16_t depthThreshold = 70;
    while (!die) {
        device.getVideo(rgbMat);
        device.getDepth(depthMat);
        cv::imshow("rgb", rgbMat);
        minDepthL = std::numeric_limits<uint16_t>::max();
        minDepthC = std::numeric_limits<uint16_t>::max();
        minDepthR = std::numeric_limits<uint16_t>::max();

        /*LEFT DEPTH PICTURE*/
        for (int i = 0; i < depthMat.rows; ++i) {
            for (int j = 0; j < depthMat.cols/3; ++j) {
                uint16_t pix = depthMat.at<uint16_t>(i, j);

                Vec3b intensity{0, 0, 0}; //blue, green, red
                if (pix == FREENECT_DEPTH_RAW_NO_VALUE) { // broken pixels
                    intensity[0] = 151;
                    intensity[1] = 0;
                    intensity[2] = 238;
                } else {
                    const int MAX_INTENSITY = 1200, MIN_INTENSITY = 300;

                    if (pix > MAX_INTENSITY) {
                        intensity[0] = 255;
                        intensity[1] = 0;
                        intensity[2] = 0;
                    }
                    else if (pix < MIN_INTENSITY) {
                        intensity[0] = 255;
                        intensity[1] = 255;
                        intensity[2] = 255;
                    }
                    else {
                        uint16_t d = 255 * (pix - MIN_INTENSITY) / (MAX_INTENSITY - MIN_INTENSITY);

                        int lb = pix/10 & 0xff;
                        switch (pix/100){
                            case 3:
                                intensity[2] = 255;
                                intensity[1] = 255-pix*0.05;
                                intensity[0] = 255-pix*0.05;
                            break;
                            case 4:
                                intensity[2] = 255;
                                intensity[1] = 255-pix*0.2;
                                intensity[0] = 255-pix*0.2;
                            break;
                            case 5:
                                intensity[2] = 255-pix/20;
                                intensity[1] = 255-pix/20;
                                intensity[0] = 0;
                            break;
                            case 6:
                                intensity[2] = 255-pix/15;
                                intensity[1] = 255-pix/15;
                                intensity[0] = 0;
                            break;
                            case 7:
                                intensity[2] = 255-pix/5;
                                intensity[1] = 255 - pix/12;
                                intensity[0] = 0;
                            break;
                            case 8:
                                intensity[2] = 0;
                                intensity[1] = 255 - pix/12;
                                intensity[0] = 0;
                            break;
                            case 9:
                                intensity[2] = 0;
                                intensity[1] = pix/8;
                                intensity[0] = 0;
                            break;
                            case 10:
                                intensity[2] = 0;
                                intensity[1] = pix/8;
                                intensity[0] = pix/12;
                            break;
                            case 11:
                                intensity[2] = 0;
                                intensity[1] = pix/12;
                                intensity[0] = pix/12;
                            break;
                            case 12:
                                intensity[2] = 0;
                                intensity[1] = pix/15;
                                intensity[0] = 255;
                            break;
                        }
                        minDepthL = std::min(d, minDepthL);
                    }
                }

                coloredDepthL.at<Vec3b>(i, j) = intensity;
            }
        }
        /*END OF LEFT DEPTH PICTURE*/

        /*CENTER DEPTH PICTURE*/
        for (int i = 0; i < depthMat.rows; ++i) {
            for (int j = depthMat.cols/3 + 1; j < 2*(depthMat.cols/3); j++) {
                uint16_t pix = depthMat.at<uint16_t>(i, j);

                Vec3b intensity{0, 0, 0}; //blue, green, red
                if (pix == FREENECT_DEPTH_RAW_NO_VALUE) { // broken pixels
                    intensity[0] = 151;
                    intensity[1] = 0;
                    intensity[2] = 238;
                } else {
                    const int MAX_INTENSITY = 1200, MIN_INTENSITY = 300;

                    if (pix > MAX_INTENSITY) {
                        intensity[0] = 255;
                        intensity[1] = 0;
                        intensity[2] = 0;
                    }
                    else if (pix < MIN_INTENSITY) {
                        intensity[0] = 255;
                        intensity[1] = 255;
                        intensity[2] = 255;
                    }
                    else {
                        uint16_t d = 255 * (pix - MIN_INTENSITY) / (MAX_INTENSITY - MIN_INTENSITY);

                        //int lb = pix/10 & 0xff;
                        switch (pix/100){
                            case 3:
                                intensity[2] = 255;
                                intensity[1] = 255-pix*0.05;
                                intensity[0] = 255-pix*0.05;
                            break;
                            case 4:
                                intensity[2] = 255;
                                intensity[1] = 255-pix*0.2;
                                intensity[0] = 255-pix*0.2;
                            break;
                            case 5:
                                intensity[2] = 255-pix/20;
                                intensity[1] = 255-pix/20;
                                intensity[0] = 0;
                            break;
                            case 6:
                                intensity[2] = 255-pix/15;
                                intensity[1] = 255-pix/15;
                                intensity[0] = 0;
                            break;
                            case 7:
                                intensity[2] = 255-pix/5;
                                intensity[1] = 255 - pix/12;
                                intensity[0] = 0;
                            break;
                            case 8:
                                intensity[2] = 0;
                                intensity[1] = 255 - pix/12;
                                intensity[0] = 0;
                            break;
                            case 9:
                                intensity[2] = 0;
                                intensity[1] = pix/8;
                                intensity[0] = 0;
                            break;
                            case 10:
                                intensity[2] = 0;
                                intensity[1] = pix/8;
                                intensity[0] = pix/12;
                            break;
                            case 11:
                                intensity[2] = 0;
                                intensity[1] = pix/12;
                                intensity[0] = pix/12;
                            break;
                            case 12:
                                intensity[2] = 0;
                                intensity[1] = pix/15;
                                intensity[0] = 255;
                            break;
                        }

                        minDepthC = std::min(d, minDepthC);
                    }
                }

                coloredDepthC.at<Vec3b>(i, j) = intensity;
            }
        }
        /*END OF CENTER DEPTH PICTURE*/

        /*RIGHT DEPTH PICTURE*/
        for (int i = 0; i < depthMat.rows; ++i) {
            for (int j = 2*(depthMat.cols/3); j <= depthMat.cols; ++j) {
                uint16_t pix = depthMat.at<uint16_t>(i, j);

                Vec3b intensity{0, 0, 0}; //blue, green, red
                if (pix == FREENECT_DEPTH_RAW_NO_VALUE) { // broken pixels
                    intensity[0] = 151;
                    intensity[1] = 0;
                    intensity[2] = 238;
                } else {
                    const int MAX_INTENSITY = 1200, MIN_INTENSITY = 300;

                    if (pix > MAX_INTENSITY) {
                        intensity[0] = 255;
                        intensity[1] = 0;
                        intensity[2] = 0;
                    }
                    else if (pix < MIN_INTENSITY) {
                        intensity[0] = 255;
                        intensity[1] = 255;
                        intensity[2] = 255;
                    }
                    else {
                        uint16_t d = 255 * (pix - MIN_INTENSITY) / (MAX_INTENSITY - MIN_INTENSITY);
                        switch (pix/100){
                            case 3:
                                intensity[2] = 255;
                                intensity[1] = 255-pix*0.05;
                                intensity[0] = 255-pix*0.05;
                            break;
                            case 4:
                                intensity[2] = 255;
                                intensity[1] = 255-pix*0.2;
                                intensity[0] = 255-pix*0.2;
                            break;
                            case 5:
                                intensity[2] = 255-pix/20;
                                intensity[1] = 255-pix/20;
                                intensity[0] = 0;
                            break;
                            case 6:
                                intensity[2] = 255-pix/15;
                                intensity[1] = 255-pix/15;
                                intensity[0] = 0;
                            break;
                            case 7:
                                intensity[2] = 255-pix/5;
                                intensity[1] = 255 - pix/12;
                                intensity[0] = 0;
                            break;
                            case 8:
                                intensity[2] = 0;
                                intensity[1] = 255 - pix/12;
                                intensity[0] = 0;
                            break;
                            case 9:
                                intensity[2] = 0;
                                intensity[1] = pix/8;
                                intensity[0] = 0;
                            break;
                            case 10:
                                intensity[2] = 0;
                                intensity[1] = pix/8;
                                intensity[0] = pix/12;
                            break;
                            case 11:
                                intensity[2] = 0;
                                intensity[1] = pix/12;
                                intensity[0] = pix/12;
                            break;
                            case 12:
                                intensity[2] = 0;
                                intensity[1] = pix/15;
                                intensity[0] = 255;
                            break;
                        }
                        minDepthR = std::min(d, minDepthR);
                    }
                }

                coloredDepthR.at<Vec3b>(i, j) = intensity;
            }
        }
        /*END OF RIGHT DEPTH PICTURE*/


        /*MAKE A CANVAS WITH ALL DEPTH*/
        for (int i = 0; i < depthMat.rows; ++i) {
            for (int j = 0; j < depthMat.cols; ++j) {
                uint16_t pix = depthMat.at<uint16_t>(i, j);

                Vec3b intensity{0, 0, 0}; //blue, green, red
                if (pix == FREENECT_DEPTH_RAW_NO_VALUE) { // broken pixels
                    intensity[0] = 151;
                    intensity[1] = 0;
                    intensity[2] = 238;
                } else {
                    const int MAX_INTENSITY = 1200, MIN_INTENSITY = 300;

                    if (pix > MAX_INTENSITY) {
                        intensity[0] = 255;
                        intensity[1] = 0;
                        intensity[2] = 0;
                    }
                    else if (pix < MIN_INTENSITY) {
                        intensity[0] = 255;
                        intensity[1] = 255;
                        intensity[2] = 255;
                    }
                    else {
                        uint16_t d = 255 * (pix - MIN_INTENSITY) / (MAX_INTENSITY - MIN_INTENSITY);
                        switch (pix/100){
                            case 3:
                                intensity[2] = 255;
                                intensity[1] = 255-pix*0.05;
                                intensity[0] = 255-pix*0.05;
                            break;
                            case 4:
                                intensity[2] = 255;
                                intensity[1] = 255-pix*0.2;
                                intensity[0] = 255-pix*0.2;
                            break;
                            case 5:
                                intensity[2] = 255-pix/20;
                                intensity[1] = 255-pix/20;
                                intensity[0] = 0;
                            break;
                            case 6:
                                intensity[2] = 255-pix/15;
                                intensity[1] = 255-pix/15;
                                intensity[0] = 0;
                            break;
                            case 7:
                                intensity[2] = 255-pix/5;
                                intensity[1] = 255 - pix/12;
                                intensity[0] = 0;
                            break;
                            case 8:
                                intensity[2] = 0;
                                intensity[1] = 255 - pix/12;
                                intensity[0] = 0;
                            break;
                            case 9:
                                intensity[2] = 0;
                                intensity[1] = pix/8;
                                intensity[0] = 0;
                            break;
                            case 10:
                                intensity[2] = 0;
                                intensity[1] = pix/8;
                                intensity[0] = pix/12;
                            break;
                            case 11:
                                intensity[2] = 0;
                                intensity[1] = pix/12;
                                intensity[0] = pix/12;
                            break;
                            case 12:
                                intensity[2] = 0;
                                intensity[1] = pix/15;
                                intensity[0] = 255;
                            break;
                        }
                    }
                }

                coloredDepthAll.at<Vec3b>(i, j) = intensity;


            }
        }



        SerialPort *serial = new SerialPort();

        static bool isObstacle = false;

        if (minDepthL < depthThreshold && minDepthC >=depthThreshold && minDepthR >=depthThreshold) {
            cout << "StopLeft: " << minDepthL << endl;
            isObstacle = false;
            // send command rotate to X angle
            serial->writeData("rt130");
            serial->writeData("rt50");
            serial->writeData("rt90");


        } else

        if (minDepthC < depthThreshold && minDepthL >=depthThreshold && minDepthR >=depthThreshold) {
            cout << "StopCenter: " << minDepthC << endl;
            isObstacle = false;
            // send command to rotate to XL and after to XR angle
            // serial->writeData("rt30");
            serial->writeData("rr40");
            serial->writeData("rt90");
            // write params to 2 vars
            // compare 2 vars
            // find best way
            // build pano
        } else

        if (minDepthR < depthThreshold && minDepthC >=depthThreshold && minDepthL >=depthThreshold) {
            cout << "StopRight: " << minDepthR << endl;
            isObstacle = false;
            serial->writeData("rt50");
            serial->writeData("rt130");
            serial->writeData("rt90");
            // send command to rotate to XL angle
            // write params to var
            // rotate to more left side
            // turn left rover
            // find best way
            // build pano
            // forward
        } else
        if (minDepthL < depthThreshold && minDepthC < depthThreshold && minDepthR>=depthThreshold) {
                cout << "StopRight: " << minDepthR << endl;
                isObstacle = false;
                //send command to ratate to XR angle
                // write params to var
                // rotate to more right side
                // turn left rover to 45 degrees
                // find best way
                // build pano
        } else
        if (minDepthC < depthThreshold && minDepthR < depthThreshold && minDepthL>=depthThreshold) {
                cout << "StopRight: " << minDepthR << endl;
                isObstacle = false;
                //send command to ratate to XL angle
                // write params to var
                // turn left rover to 45 degrees
                // scan again
                // find best way
                // build pano

        } else
        if (minDepthL < depthThreshold && minDepthC < depthThreshold && minDepthR < depthThreshold) {
            cout << "StopRight: " << minDepthR << endl;
            if (!isObstacle){
               serial->writeData("mv");
                    isObstacle = true;
                }

        } else
        if (minDepthL < depthThreshold && minDepthR < depthThreshold && minDepthC>=depthThreshold) {
                cout << "CENTER IS CLEAR. SLOW SPEED" << endl;
                isObstacle = false;
                //need to find blocks in sides, so using lidar to get the distance btw blocks.
                //set speed to 128 of 256
                //forward 20 sm, after scan again
        } else {
            cout << "CENTER IS CLEAR. SLOW SPEED" << endl;
            isObstacle = false;
            //send the rover formward for just 1 meter. Afterwards, scan again
        }


        //SHOW DEPTH PICTURERS
        cv::imshow("depth", coloredDepthAll);
        depthfL = coloredDepthL;
        depthfC = coloredDepthC;
        depthfR = coloredDepthR;
        //depthMat = coloredDepthAll;

        char k = cvWaitKey(5);
        if( k == 27 ){ //ESC
            cv::destroyAllWindows();
            break;
        }

        if( k == 32 ) { //SPACE
            if(i_snap_rgb > 999)
                i_snap_rgb = 0;
            if(i_snap_dpt > 999)
                i_snap_dpt = 0;

            takePict();
            takePictReal();

        }
    }

    device.stopVideo();
    device.stopDepth();
    exit(0);
}



void taking::takePictReal(){
    std::ostringstream file_real_rgb;
    file_real_rgb << filename_real_rgb <<  suffix;
    cv::imwrite(file_real_rgb.str(),rgbMat);

    std::ostringstream file_real_dpt1;
    std::ostringstream file_real_dpt2;
    std::ostringstream file_real_dpt3;
    std::ostringstream file_ALL;
    /*file_real_dpt1 << filename_real_dpt << "Left" << suffix;
    cv::imwrite(file_real_dpt1.str(),depthfL);

    file_real_dpt2 << filename_real_dpt << "Cent" << suffix;
    cv::imwrite(file_real_dpt2.str(),depthfC);

    file_real_dpt3 << filename_real_dpt << "Right" << suffix;
    cv::imwrite(file_real_dpt3.str(),depthfR);
    */

    file_ALL << "RealDepth" << suffix;
    cv::imwrite(file_ALL.str(),depthMat);

    contour->countors_rgb("RealRGB.png", "RealContRGB.jpg");
    //oc->countors_dpt("RealDpt.jpg", "RealContDpt.jpg");
}


void taking::takePict(){
    std::ostringstream file_rgb;
    file_rgb << filename_rgb << i_snap_rgb << suffix;
    cv::imwrite(file_rgb.str(),rgbMat);
    i_snap_rgb++;

//    std::ostringstream file_real_dpt1;
//    std::ostringstream file_real_dpt2;
//    std::ostringstream file_real_dpt3;
//    file_real_dpt1 << filename_real_dpt << "Left_" << i_snap_dpt << suffix;
//    cv::imwrite(file_real_dpt1.str(),depthfL);

//    file_real_dpt2 << filename_real_dpt << "Cent_" << i_snap_dpt << suffix;
//    cv::imwrite(file_real_dpt2.str(),depthfC);

//    file_real_dpt3 << filename_real_dpt << "Right_" << i_snap_dpt << suffix;
//    cv::imwrite(file_real_dpt3.str(),depthfR);
    i_snap_dpt++;
}
