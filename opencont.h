#pragma once
#ifndef OPENCONT_H
#define OPENCONT_H

#include <QObject>
#include "libfreenect/wrappers/cpp/libfreenect.hpp"
#include <iostream>
#include <vector>
#include <cmath>
#include <pthread.h>
//#include <opencv/cv.h>
//#include <opencv/cxcore.h>
//#include <opencv/highgui.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/video/video.hpp"
#include "opencv2/opencv_modules.hpp"

using namespace cv;

class opencont
{
public:
    opencont();
    void countors_rgb(char *filename, char *savefile);
    void countors_dpt(char *filename, char *savefile);
    void minusCont(char* filename);
    void minusContDpt(char* filename);
    void EnclosingCircle(IplImage* _image);
};

#endif // OPENCONT_H
