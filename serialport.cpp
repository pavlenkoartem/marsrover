#pragma once
#include <QObject>
#include "taking.h"
#include "serialport.h"

using namespace std;

SerialPort::SerialPort()
{
    serial = new QSerialPort();

    connect(serial, SIGNAL(readyRead()), this, SLOT(readData()));
    connect(this,SIGNAL(readDataComplete(QString)),this,SLOT(commandParser(QString)));
}

void SerialPort::createSerialList()
{
    if(!QSerialPortInfo::availablePorts().isEmpty()){
        foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
            serialList.append(info.portName());
        }
        tuneSerial();
    } else {
        cout << "[Serial] There're no ports available" << endl;
    }

}

void SerialPort::tuneSerial()
{
    if (!QSerialPortInfo::availablePorts().isEmpty()){
        cout << "[Serial] Ready to open" << endl;
        serial->setPortName("/dev/ttyACM0");
        serial->setBaudRate(QSerialPort::Baud57600);
    }
    else
    {
        return;
    }
    serial->open(QIODevice::ReadWrite);
    if(serial->isOpen() && serial->isReadable()){
        cout << "[Serial] Open COM" << endl;
    } else {
        cout << "[Serial] Can't open COM" << endl;
    }
}

void SerialPort::writeData(QString str)
{
    QByteArray byteArray;
    byteArray = str.toUtf8();
    if(serial->isOpen() && serial->isWritable()){
        serial->write(byteArray);
        cout << "[Serial] Data written: " << str.toStdString() << endl;
    }
}
int i = 0;
void SerialPort::readData()
{
        byteArray.clear();
        byteArray += serial->readAll();
        QString newstring=QString::fromUtf8(byteArray.constData()); //making a string to reading data into
        cout << "[Serial] " << newstring.toStdString() << endl;
        emit readDataComplete(newstring);
}

void SerialPort::commandParser(QString text){
    if(text == "tkrlph\r\n" || text == "tkrlph"){
        emit commandTakePhoto();
        taking *getPicture = new taking();
        getPicture->takePictReal();
        getPicture->takePict();

    } else if(text == "tkph\r\n" || text == "tkph"){
        taking *getPicture = new taking();
        getPicture->takePict();
    } else if(text == "programming\r\n" || text == "programming"){
        auto name = QStringLiteral("%1.txt").arg(number);
        emit readyToProgram(name);
        number++;
    }

    else return;
}
