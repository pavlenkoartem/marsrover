#pragma once
#include <iostream>
#include <vector>
#include <cmath>
#include <pthread.h>
#include <opencv2/opencv.hpp>
#include <QCoreApplication>
#include <QThread>
#include <QObject>
#include <QTimer>
#include "libfreenect/wrappers/cpp/libfreenect.hpp"
#include "taking.h"
#include "opencont.h"
#include "serialport.h"
using namespace cv;
using namespace std;


int main(int argc, char **argv) {
    QCoreApplication a(argc, argv);

    SerialPort *serial = new SerialPort();
    serial->createSerialList();

    taking *tp = new taking();
    tp->videoLoop();


    delete(tp);
    delete(serial);
    return a.exec();
}

