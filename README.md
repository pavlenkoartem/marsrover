**This is a source code of my robotics project "The prototype of Mars rover"**

It is not a fully working version of the code, there's just 40% from all.

This project contains the following libs: libusb, libopencv, libfreenect.

The main goal of the project is to detect obstacles using a depth sensor and an RGB-camera (Kinect) 
and send data to AVR based self-assembled controller (atmega2560) via UART 

Designed with Qt
