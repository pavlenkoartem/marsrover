#pragma once
#ifndef SERIALPORT_H
#define SERIALPORT_H

#include <QCoreApplication>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QThread>
#include <QList>
#include <QObject>
#include <QTextStream>
#include <QDebug>
#include <QEventLoop>
using namespace std;
class SerialPort : public QObject
{
    Q_OBJECT
public:
    SerialPort();
    void createSerialList();
    void tuneSerial();
    int number = 0;


    QByteArray byteArray;
    bool flag = false;

private:
    QSerialPort *serial;
    QList<QString> serialList;


public slots:
    void readData();
    void commandParser(QString text);
    void writeData(QString str);

signals:
    void readDataComplete(QString);
    void commandTakePhoto();
    void commandGetInfo();
    void readyToProgram(QString);

};

#endif // SERIALPORT_H
