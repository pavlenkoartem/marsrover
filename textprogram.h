#pragma once
#ifndef TEXTPROGRAM_H
#define TEXTPROGRAM_H

#include <QCoreApplication>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QThread>
#include <QList>
#include <QObject>
#include <QTextStream>
#include <QDebug>
#include <QEventLoop>
#include <QString>
using namespace std;
class textprogram : public QObject
{
    Q_OBJECT
public:
    textprogram();

signals:

public slots:

    void createFile(QString name);
    void openFile(QString name);
};

#endif // TEXTPROGRAM_H
