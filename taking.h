#pragma once
#ifndef TAKING_H
#define TAKING_H
#include <iostream>
#include <vector>
#include <cmath>
#include <pthread.h>
#include <opencv/cv.h>
#include <opencv/cxcore.h>
#include <opencv/highgui.h>
#include "libfreenect/wrappers/cpp/libfreenect.hpp"

#include <QObject>
#include <QCoreApplication>

#include "opencont.h"
#include "serialport.h"

using namespace cv;
using namespace std;

class taking: public QObject
{
    Q_OBJECT
public:
    taking();

    int i_snap_rgb = 0;
    int i_snap_dpt = 0;
    const string filename_rgb = "Rgb";
    const string filename_dpt = "Dpt";
    const string suffix = ".png";

    void videoLoop();
private:
    opencont *contour;

public slots:
    void takePict();
    void takePictReal();
};

#endif // TAKING_H
